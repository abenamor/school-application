### What is this repository for? ###

* A school management application using spring boot.

### This project was made for training purpose only in order to initiate to spring boot. ###

It offers many services as:  
- Add, edit, delete a student (Forms validation).  
- Get the student list.  

### How do I get set up?
* Create a data base as name schoolDB.
* Clone the project then run it as spring boot application.  

### Youtube video

Here is a descriptive video in which I demonstrate a full user story:  
[school application video](https://youtu.be/mfGU3P0K16U)