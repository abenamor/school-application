package org.talan;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.talan.dao.StudentRepository;
import org.talan.entities.Student;

/**
 * Description of file SchoolApplication.java <br>
 * 
 * @author adem
 * 
 * This project is demo to train in building spring boot application <br>
 * 
 * This is a manage student application.
 *
 */

@SpringBootApplication
public class SchoolApplication {

	public static void main(String[] args) throws ParseException {
		ApplicationContext springContext = SpringApplication.run(
				SchoolApplication.class, args);
		StudentRepository studentRepositoryImp = springContext
				.getBean(StudentRepository.class);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		/*studentRepositoryImp.save(new Student("adem", dateFormat
				.parse("1993-08-31"), "adem@gmail.com", "adem.jpg"));
		studentRepositoryImp.save(new Student("oussama", dateFormat
				.parse("1990-05-20"), "oussama@gmail.com", "oussama.jpg"));
		studentRepositoryImp.save(new Student("asma", dateFormat
				.parse("1992-05-01"), "asma@gmail.com", "asma.jpg"));
		studentRepositoryImp.save(new Student("zeineb", dateFormat
				.parse("1975-05-01"), "zeineb@gmail.com", "zeineb.jpg"));
		studentRepositoryImp.save(new Student("kais", dateFormat
				.parse("1994-05-01"), "kais@gmail.com", "kais.jpg"));
		studentRepositoryImp.save(new Student("wassim", dateFormat
				.parse("1996-05-01"), "wassim@gmail.com", "wassim.jpg"));
		studentRepositoryImp.save(new Student("balkis", dateFormat
				.parse("1995-05-01"), "balkis@gmail.com", "balkis.jpg"));
		studentRepositoryImp.save(new Student("nadia", dateFormat
				.parse("1988-05-01"), "nadia@gmail.com", "nadia.jpg"));*/

		// Page<Student> studentPage = studentRepositoryImp
		// .findAll(new PageRequest(0, 5));

		Page<Student> studentPage = studentRepositoryImp.findStudents("%a%",
				(new PageRequest(0, 44)));
		studentPage.forEach(s -> System.out.println(s.getName()));

	}
}
