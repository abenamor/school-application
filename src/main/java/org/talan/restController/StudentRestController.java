package org.talan.restController;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.talan.dao.StudentRepository;
import org.talan.entities.Student;

/**
 * Description of file StudentRestController.java <br>
 * 
 * @author adem
 * 
 *         It is used to expose the student resource
 *
 */
@RestController
public class StudentRestController {

	@Autowired
	private StudentRepository studentRepository;

	@RequestMapping("/students")
	public List<Student> getAllStudents() {
		return studentRepository.findAll();
	}

	@RequestMapping("/students/{id}")
	public Student getStudentById(@PathParam("id") Long id) {
		return studentRepository.findOne(id);
	}

	@RequestMapping(value = "/students/{id}", method = RequestMethod.DELETE)
	public void deleteStudent(@PathVariable(value = "id") Long id) {
		studentRepository.delete(id);
	}

}
