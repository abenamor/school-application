package org.talan.controller;

import java.io.File;
import java.io.FileInputStream;

import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.talan.dao.StudentRepository;
import org.talan.entities.Student;

/**
 * Description of file: StudentController.java
 * 
 * @author adem
 * 
 *         It represents the Controller of the spring MVC framework <br>
 *         It is used to intercepts the user's requests, create model and return
 *         the view logic name <br>
 *
 */
@Controller
@RequestMapping(value = "/studentController")
public class StudentController {
	@Autowired
	private StudentRepository studentRepository;
	@Value("${images.dir}")
	private String imagesDir;

	@RequestMapping(value = "/students", method = RequestMethod.GET)
	public String getStudents(Model model, @RequestParam(name = "page", defaultValue = "0") int p,
			@RequestParam(name = "keyWord", defaultValue = "") String kw) {
		// Page<Student> studentPage = studentRepository.findAll(new
		// PageRequest(
		// p, 5));
		Page<Student> studentPage = studentRepository.findStudents("%" + kw + "%", new PageRequest(p, 3));
		int pageNumber = studentPage.getTotalPages();
		int[] pages = new int[pageNumber];
		for (int i = 0; i < pageNumber; i++)
			pages[i] = i;
		model.addAttribute("pagesModel", pages);
		model.addAttribute("studentsPageModel", studentPage);
		model.addAttribute("currentPageModel", p);
		model.addAttribute("keyWordModel", kw);
		return "students";
	}

	@RequestMapping(value = "/toStudentForm", method = RequestMethod.GET)
	public String toStudentForm(Model model) {
		model.addAttribute("studentModel", new Student());
		return "studentForm";
	}

	@RequestMapping(value = "/saveStudent", method = RequestMethod.POST)
	public String saveStudent(@Valid @ModelAttribute("studentModel") Student student, BindingResult bindingResult,
			@RequestParam(name = "picture") MultipartFile file) throws Exception {

		if (bindingResult.hasErrors()) {
			return "studentForm";
		}
		if (!file.isEmpty())
			student.setPhoto(file.getOriginalFilename());
		studentRepository.save(student);
		if (!file.isEmpty()) {
			student.setPhoto(file.getOriginalFilename());
			// file.transferTo(new
			// File(System.getProperty("user.home")+"/schoolServer/"+file.getOriginalFilename()));
			file.transferTo(new File(imagesDir + student.getId()));
		}

		// model.addAttribute("studentModel", new Student());
		return "redirect:students";
	}

	@RequestMapping(value = "/getPhoto", produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] getPhoto(Long studentId) throws Exception {
		File file = new File(imagesDir + studentId);
		return IOUtils.toByteArray(new FileInputStream(file));

	}

	@RequestMapping(value = "/removeStudent")
	public String removeStudent(Long studentId) {
		studentRepository.delete(studentId);
		return "redirect:students";

	}

	@RequestMapping(value = "/toStudentEditForm")
	public String editStudent(Long studentId, Model model) {
		Student student = studentRepository.findOne(studentId);
		model.addAttribute("studentEditModel", student);

		return "studentEditForm";
	}

	@RequestMapping(value = "/updateStudent", method = RequestMethod.POST)
	public String updateStudent(@Valid @ModelAttribute("studentEditModel") Student student, BindingResult bindingResult,
			@RequestParam(name = "picture") MultipartFile file) throws Exception {

		if (bindingResult.hasErrors()) {
			return "studentEditForm";
		}
		if (!file.isEmpty()) {
			// set student photo name before saving it
			student.setPhoto(file.getOriginalFilename());
		}
		studentRepository.save(student);
		if (!file.isEmpty()) {
			// student.setPhoto(file.getOriginalFilename());
			// file.transferTo(new
			// File(System.getProperty("user.home")+"/schoolServer/"+file.getOriginalFilename()));
			file.transferTo(new File(imagesDir + student.getId()));
		}

		// model.addAttribute("studentModel", new Student());
		return "redirect:students";
	}

}
