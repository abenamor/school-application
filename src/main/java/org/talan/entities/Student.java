package org.talan.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Description of file: Student.java <br>
 * 
 * @author adem
 *
 *         This class provide a student information
 */
@Entity
public class Student implements Serializable {

	@Id
	@GeneratedValue()
	private Long id;
	@Column(length = 40)
	@NotEmpty(message = "The name is required")
	@Size(min = 4, max = 35)
	private String name;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthDate;
	@Column(length = 40)
	@Size(min = 12, max = 100)
	@NotEmpty
	@Email
	private String email;
	@Column(length = 40)
	private String photo;

	public Student() {
		super();
	}

	public Student(Long id, String name, Date birthDate, String email, String photo) {
		super();
		this.id = id;
		this.name = name;
		this.birthDate = birthDate;
		this.email = email;
		this.photo = photo;
	}

	public Student(String name, Date birthDate, String email, String photo) {
		super();
		this.name = name;
		this.birthDate = birthDate;
		this.email = email;
		this.photo = photo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", birthDate=" + birthDate + ", email=" + email + ", photo="
				+ photo + "]";
	}

}
