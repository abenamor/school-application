package org.talan.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.talan.entities.Student;

/**
 * Description of file: StudentRepository.java <br>
 * 
 * @author adem
 *
 *         This interface is an extension of JpaRepository Interface that is
 *         provided by spring data for implementing the access data layer <br>
 */
public interface StudentRepository extends JpaRepository<Student, Long> {

	public List<Student> findByName(String name);

	public Page<Student> findByName(String name, Pageable page);

	@Query("select s from Student s where s.name like :x")
	public Page<Student> findStudents(@Param("x") String name, Pageable page);

	@Query("select s from Student s where s.birthDate > :x and s.birthDate < :y ")
	public List<Student> findStudents(@Param("x") Date minDate, @Param("y") Date maxDate);

}
